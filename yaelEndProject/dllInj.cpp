// Skeleton code - inject DLL to a running process
#include <Windows.h>
#include "tlhelp32.h"
#include <iostream>
#include <stdlib.h>
#include <string>
using namespace std;
#define BUFSIZE 1000
#define _CRT_SECURE_NO_WARNINGS
#define DLL_PATH "C:\\Users\\user\\Documents\\magshimim\\secondYaer\\Semester B\\architecture\\mydll\\Release\\mydll.dll"
int main()

{
	
	const WCHAR* procName = L"notepad.exe";
	DWORD procID;
	DWORD err;
	BOOL check;
	HANDLE  proc;
	LPCSTR dllPath = "C:\\Users\\user\\Documents\\magshimim\\secondYaer\\Semester B\\architecture\\mydll\\Release\\mydll.dll";
	TCHAR  buffer[BUFSIZE];
	int i=0;
	WCHAR arr[1000];

	PROCESSENTRY32 procEntry;
	procEntry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	// Get full path of DLL to inject
	DWORD pathLen = GetFullPathNameA(dllPath, BUFSIZE, buffer, NULL);

	// Get LoadLibrary function address �
	// the address doesn't change at remote process
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
	
	//finding the processId
	if (Process32First(snapshot, &procEntry))
	{
		do
		{
			while (procEntry.szExeFile[i] != -52)
			{
				arr[i] = procEntry.szExeFile[i];
				i++;
			}
			i = 0;
			if (!wcscmp(arr, procName))
			{
				wprintf(L"[+] The process  was found in memory.\n");
				procID = procEntry.th32ProcessID;
				// Open remote process
				proc = OpenProcess(PROCESS_CREATE_THREAD|PROCESS_QUERY_INFORMATION|PROCESS_VM_WRITE|PROCESS_VM_OPERATION, FALSE, procID);
				if (proc == NULL)
				{
					printf("[---] Failed to open process %s.\n", procEntry.szExeFile);
					system("pause");
					return 0;
				}
				else
				{
					break;
				}

			}
		} while (Process32Next(snapshot, &procEntry));
	}


//---------------------------------------------------------------------------------------------------------------

	


	// Get a pointer to memory location in remote process,
	// big enough to store DLL path
	PVOID memAddr = (PVOID)VirtualAllocEx(proc, NULL, strlen(dllPath), MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (NULL == memAddr) 
	{
		err = GetLastError();
		return 0;
	}
	// Write DLL name to remote process memory
	check = WriteProcessMemory(proc, memAddr, dllPath, strlen(dllPath), NULL);
	if (0 == check)
	{
		err = GetLastError();
		return 0;
	}
	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
		HANDLE hRemote = CreateRemoteThread(proc, NULL,NULL, (LPTHREAD_START_ROUTINE)addrLoadLibrary, memAddr,NULL,NULL);
	if (NULL == hRemote) {
		err = GetLastError();
		return 0;
	}
	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	return 0;
	system("pause");
}
